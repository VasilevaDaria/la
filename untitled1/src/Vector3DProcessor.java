public class Vector3DProcessor {

     // Метод возращает сумму двух векторов как новый вектор

    static Vector3D sumVector(Vector3D s1, Vector3D s2) {
        return new Vector3D(s1.getX() + s2.getX(), s1.getY() + s2.getY(), s1.getZ() + s2.getZ());
    }


     //Метод возращает разность двух векторов как новый вектор

    static Vector3D subVector(Vector3D s1, Vector3D s2) {
        return new Vector3D(s1.getX() - s2.getX(), s1.getY() - s2.getY(), s1.getZ() - s2.getZ());
    }


     // Метод вычисляет скалярное произведение двух векторов

    static double scalarVector(Vector3D s1, Vector3D s2) {
        return s1.getX() * s2.getX() + s1.getY() * s2.getY() + s1.getZ() * s2.getZ();
    }

// Метод вычисляет векторное произведение двух векторов

    static Vector3D vectorProduct(Vector3D s1, Vector3D s2) {
        return new Vector3D(s1.getY() * s2.getZ() - s1.getZ() * s2.getY(),
                -(s1.getX() * s2.getZ() - s1.getZ() * s2.getX()),
                s1.getX() * s2.getY() - s1.getY() * s2.getX());
    }

    // Метод определяет коллинеарны ли 2 вектора

    static boolean collVector(Vector3D s1, Vector3D s2) {
        return s1.getX() * s2.getY() - s2.getX() * s1.getY() == 0 && s1.getX() * s2.getZ() - s1.getZ() * s2.getX() == 0
                && s1.getY() * s2.getZ() - s1.getZ() * s2.getY() == 0;
    }
}
