import java.util.Scanner;

public class lab14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size;
        System.out.println("Введите количество элементов массива");
        size = in.nextInt();
        int array[] = new int[size];
        System.out.println("Введите элементы массива");
        for (int i = 0; i < size; i++) {
            array[i] = in.nextInt();
        }
        System.out.println("Вывод массива");
        for (int i = size - 1; i >= 0; i--) {
            System.out.print(" " + array[i]);
        }
    }
}
