import java.util.Scanner;

public class lab13 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int size;
        int count = 0;
        int sum = 0;
        System.out.println("Введите количество элементов массива");
        size = in.nextInt();
        int array[] = new int[size];
        System.out.println("Введите элементы массива");
        for (int i = 0; i < size; i++) {
            array[i] = in.nextInt();
        }
        for (int i = 0; i < size; i++) {
            if (array[i] <= 0) {
                count++;
            }
        }
        if (count == 0) {
            System.out.print(" Все элементы положительные ");
        } else {
            System.out.print(" Не все элементы положительные");
        }
    }
}
