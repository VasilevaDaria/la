import java.util.Scanner;

public class lab7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите точность вычесления E = ");
        double E = scan.nextDouble();
        System.out.print("Введите x = ");
        double x = scan.nextDouble();
        double current = 1.0, s = 1.0;
        int n = 1;
        do {
            s = s + Math.pow(x, n) / Factorial(n);
            current = Math.pow(x, n) / Factorial(n);
            n++;
        } while (Math.abs(current) > E);
        System.out.printf(" Значение функции exp()= %s", s);
        if ((s - Math.exp(n)) < E) {
            System.out.println("OK");
        } else {
            System.out.println("No equal");
        }
    }

    public static long Factorial(long number) {
        long fact = 1;
        for (int i = 1; i <= number; i++) {
            fact *= i;

        }
        return fact;
    }
}

