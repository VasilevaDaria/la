import java.util.Scanner;

public class lab6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float a, b, c, d, e, f;
        System.out.println("программа решает систему линейных уравнений с двумя неизвестными");
        System.out.println("Введите переменную a");
        a = in.nextFloat();
        System.out.println("Введите переменную b");
        b = in.nextFloat();
        while ((a == 0) && (b == 0)) {
            System.out.println("a и b не должены одновременно ровнятся нулю. Введите переменную a");
            a = in.nextFloat();
            System.out.println("Введите переменную b");
            b = in.nextFloat();
        }
        System.out.println("Введите переменную c");
        c = in.nextFloat();
        System.out.println("Введите переменную d");
        d = in.nextFloat();
        System.out.println("Введите переменную e");
        e = in.nextFloat();
        while ((d == 0) && (e == 0)) {
            System.out.println("e и d не должены одновременно ровнятся нулю. Введите переменную e");
            e = in.nextFloat();
            System.out.println("Введите переменную d");
            d = in.nextFloat();
        }
        System.out.println("Введите переменную f");
        f = in.nextFloat();
        if (((-b * d) + (e * a)) == 0) {
            System.out.println(" Решения нет");
        } else {
            float x, y;
            x = ((e * c) - (b * f)) / ((-b * d) + (e * a));
            y = ((f * a) - (c * d)) / ((-b * d) + (e * a));

            System.out.println("x = " + x + "  y = " + y + "");

        }

    }
}
