import java.util.Objects;
public class Product {
    //Класс «Товар». Товар имеет название и описание (строки)
    private String name;
    private String description;
    public Product(String name, String description) throws ProductException{
        if(name.isEmpty() || description.isEmpty()){
            throw new ProductException("Нет названия или описания товара");
        }
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription(){
        return description;
    }

    public void setName(String name) throws ProductException{
        if(name.isEmpty()){
            throw new ProductException("Нет названия товара");
        }
        this.name = name;
    }

    public void setDescription(String description) throws ProductException{
        if(description.isEmpty()){
            throw new ProductException("Нет описания товара");
        }
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) &&
                Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
