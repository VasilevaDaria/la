import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductServiceTest {
    @Test
    void countByFilter() throws PackingException, ProductException, FilterException {
        MyPackingWeightProduct[] arr = new  MyPackingWeightProduct[4];

        Packing pack = new Packing("Упаковка 1", 0.001);
        Packing pack1 = new Packing("Упаковка 2", 0.002);

        Product goods = new Product("Товар", "Описание");
        PieceProduct pieceProduct = new PieceProduct("товар", "Упаковка и его описание", 0.2);
        arr[0] = new PackingPieceProduct(pack,3, pieceProduct);
        arr[1] = new PackingWeightProduct(goods, 0.5, pack1);
        arr[2] = new PackingPieceProduct(pack,5, pieceProduct);
        arr[3] = new PackingPieceProduct(pack1, 6, pieceProduct);
        Shipment shipment = new Shipment("Сундук", arr);
        BeginStringFilter filt = new BeginStringFilter("Упаковка");
        assertEquals(3,ProductService.countByFilter(shipment,filt));
    }
}