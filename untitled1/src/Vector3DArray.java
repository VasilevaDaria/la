public class Vector3DArray {
    private Vector3D[] arr;


     // Конструктор по размеру (создает массив из нулевых векторов)

    public Vector3DArray(int length) {
        arr = new Vector3D[length];
        for (int i = 0; i < length; i++) {
            arr[i] = new Vector3D();
        }
    }

  // Возращает длину массива

    public int getLength() {
        return arr.length;
    }

    //Замена i-го элемента массива на заданный вектор

    public void editElem(int index, Vector3D vector) {
        arr[index] = vector;
    }

    // Метод находит вектор с наибольшой длиной

    public double findMax() {
        double max = arr[0].getLen();
        for (Vector3D elem : arr) {
            if (max < elem.getLen()) {
                max = elem.getLen();
            }
        }
        return max;
    }

    // Поиск заданного вектора в массиве


    public int findEqualsIndex(Vector3D vector) {
        for (int i = 0; i < arr.length; i++) {
            if (vector.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    //Вычисляет сумму всех длин векторов в массиве

    public double sumAllVector() {
        double res = 0;
        for (Vector3D elem : arr) {
            res += elem.getLen();
        }
        return res;
    }


     // Метод, вычисляет линейную комбинацию векторов с заданными коэффициентами.

    public Vector3D combVector(Double[] odds) throws IllegalArgumentException {
        if (arr.length == odds.length) {
            double x = 0, y = 0, z = 0;
            for (int i = 0; i < arr.length; i++) {
                x += odds[i] * arr[i].getX();
                y += odds[i] * arr[i].getY();
                z += odds[i] * arr[i].getZ();
            }
            return new Vector3D(x, y, z);
        } else {
            throw new IllegalArgumentException("No Enough len");
        }
    }

    //Метод вычисляет массив точек, каждая из которых – результат сдвига точки P, на соответствующий вектор.

    public Point3D[] shiftVector(Point3D s1) {
        Point3D[] res = new Point3D[arr.length];
        for (int i = 0; i < arr.length; i++) {
            res[i] = new Point3D(this.arr[i].getX() + s1.getX(),
                    this.arr[i].getY() + s1.getY(),
                    this.arr[i].getZ() + s1.getZ());
        }
        return res;
    }
}
