import java.util.Arrays;
import java.util.Objects;

public class Payment {
    private  String fullName;
    private int month, day, year;
    private int sum;

    Payment(String fio, int month, int day, int year, int amount){
        this.fullName = fio;
        this.month = month;
        this.day = day;
        this.year = year;
        this.sum = amount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setMonth(int month){
        this.month = month;
    }

    public int getMonth(){
        return month;
    }

    public void setDay(int day){
        this.day = day;
    }

    public int getDay(){
        return day;
    }

    public void setYear(int year){
        this.year = year;
    }

    public int getYear(){
        return year;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getSum() {
        return sum;
    }
    // сравнивает два платежа
    public boolean equals(Payment payment) {
        if (this == payment) {
            return true;
        }
        if (payment == null || getClass() != payment.getClass()) {
            return false;
        }
        return month == payment.month && day == payment.day
                && year == payment.year && sum == payment.sum && payment.fullName.equals(fullName);
    }
    //преобразует все данные платежа в одну строку
    @Override
    public String toString(){
        return String.format("Плательщик: %s D/M/Y:%d/%d/%d Сумма: %d руб. %d коп.", fullName, day, month, year,
                sum/100, sum % 100);
    }
    // значение фиксированной длины
    @Override
    public int hashCode() {
        return Objects.hash(fullName, month, day, year, sum);
    }
}
