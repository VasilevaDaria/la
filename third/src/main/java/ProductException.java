public class ProductException extends Exception {
    double number;
    public ProductException(String message){
        super(message);
    }
    public ProductException(String message, double num){
        super(message);
        number = num;
    }
    public double getNumber(){
        return number;
    }
}
