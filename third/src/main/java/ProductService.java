public class ProductService {
    //Метод, который получает на вход партию товара и произвольный фильтр
    public static int countByFilter(Shipment shipment, Filter filter) throws FilterException {
        int counter = 0;
        String tempDescription;
        for(MyPackingWeightProduct temp: shipment.getShipment()){
            tempDescription = temp.getProduct().getDescription();
            if(filter.apply(tempDescription)){
                counter++;
            }
        }
        return counter;
    }
}
