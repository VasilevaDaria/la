public class StringProcessor {
    /* задание первое
    На входе строка s и целое число N. Выход — строка, состоящая из N копий строки s,
    записанных подряд. При N = 0 результат — пустая строка. При N < 0 выбрасывается исключение.  */
    public static String Сopy(String s, int n) throws IllegalArgumentException {
        if (n > 0) {
            StringBuilder res = new StringBuilder();

            for (int i = 0; i < n; i++) {
                res.append(s);
            }
            return res.toString();
        }
        else if (n == 0) {
            return "";
        }
        else {
            throw new IllegalArgumentException("Negative value of repeat - impossible");
        }
    }

    /* задание 2
     На входе две строки. Результат — количество вхождений второй строки в первую
    */
    public static int number(String s, String subs) throws IllegalArgumentException{
        if (subs.length() > s.length()) {
            throw new IllegalArgumentException("Too long substring");
        } else if (s.equals(subs)) {
            return 1;
        } else {
            int counter = 0;
            int subsIndex = 0;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == subs.charAt(subsIndex)) {
                    subsIndex++;
                    if (subsIndex == subs.length()) {
                        counter++;
                        subsIndex = 0;
                    }
                }
            }
            return counter;
        }
    }

    /* задание 3
    Постройте по строке новую строку, которая получена из исходной заменой каждого символа '1' на подстроку ”один”,
    символа ‘2’ на подстроку “два” и символа ‘3’ на подстроку “три”.  */
    public static String replace(String s) {
        StringBuilder s1 = new StringBuilder();
        int tempChar;

        for (int i = 0; i < s.length(); i++) {
            tempChar = s.charAt(i);
            if (tempChar == 49) {
                s1.append("один");
            } else if (tempChar == 50) {
                s1.append("два");
            } else if (tempChar == 51) {
                s1.append("три");
            } else {
                s1.append(s.charAt(i));
            }
        }

        return s1.toString();


    }

    /* задание 4
    В строке типа StringBuilder удалите каждый второй по счету символ. Должна быть модифицирована входная строка,
     а не порождена новая.
    */
    public static StringBuilder deleteSymbol(StringBuilder s) {
        for (int i = 0; i < s.length(); i++) {
            s.delete(i, i + 1);
        }
        return s;
    }
}