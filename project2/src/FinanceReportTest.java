
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class FinanceReportTest {
    @Test
    void testGetPaymentCount() {
        Payment[] payments = {new Payment("А.А.Иванов", 1,2, 2000, 10000),
                new Payment("А.А.Иванов", 2,2, 2000, 10000),
                new Payment("Б.Г.Большаков", 1,2, 2001, 150000),
                new Payment("А.А.Иванов", 2,2, 2001, 150000),
        };
        FinanceReport test = new FinanceReport(payments, "Д.Г.Данилов", new Date());
        assertEquals(4, test.getPaymentCount());
    }

    @Test
    void testEditPayment() {
        Payment[] payments = {new Payment("А.А.Иванов", 1,2, 2000, 10000),
                new Payment("А.А.Иванов", 2,2, 2000, 10000),
                new Payment("Б.Г.Большаков", 1,2, 2001, 150000),
                new Payment("А.А.Иванов", 2,2, 2001, 150000),
        };
        FinanceReport test = new FinanceReport(payments, "Д.Г.Данилов", new Date());
        test.editPayment(1,"А.А.Иванов", 2,2, 2000, 15000);
        test.editPayment(2, new Payment("Б.Г.Большаков", 1,2, 2001, 15000));

        assertEquals("Плательщик: А.А.Иванов D/M/Y:2/2/2000 Сумма: 150 руб. 0 коп.",
                test.getPayment(1).toString());
        assertEquals("Плательщик: Б.Г.Большаков D/M/Y:2/1/2001 Сумма: 150 руб. 0 коп.",
                test.getPayment(2).toString());
    }

    @Test
    void testEditPaymentWithIncorrectParameter(){
        Payment[] payments = {new Payment("А.А.Иванов", 1,2, 2000, 10000),
                new Payment("А.А.Иванов", 2,2, 2000, 10000),
                new Payment("Б.Г.Большаков", 1,2, 2001, 150000),
                new Payment("А.А.Иванов", 2,2, 2001, 150000),
        };
        FinanceReport test = new FinanceReport(payments, "Д.Г.Данилов", new Date());
        assertThrows(IllegalArgumentException.class,()->
                test.editPayment(4,"1",1,2,2000, 1));
    }

    @Test
    void testToString() {
        Payment[] payments = {new Payment("А.А.Иванов", 1,2, 2000, 10000),
                new Payment("А.А.Иванов", 2,2, 2000, 10000),
                new Payment("Б.Г.Большаков", 1,2, 2001, 150000),
                new Payment("А.А.Иванов", 2,2, 2001, 150000),
        };
        FinanceReport test = new FinanceReport(payments, "Д.Г.Данилов", new Date(1));
        assertEquals("[Автор:Д.Г.Данилов, Дата:Thu Jan 01 06:00:00 OMST 1970, Платежи:[\n" +
                "Плательщик: А.А.Иванов D/M/Y:2/1/2000 Сумма: 100 руб. 0 коп.\n" +
                "Плательщик: А.А.Иванов D/M/Y:2/2/2000 Сумма: 100 руб. 0 коп.\n" +
                "Плательщик: Б.Г.Большаков D/M/Y:2/1/2001 Сумма: 1500 руб. 0 коп.\n" +
                "Плательщик: А.А.Иванов D/M/Y:2/2/2001 Сумма: 1500 руб. 0 коп.\n" +
                "]]", test.toString());

    }

    @Test
    void testClone() {
        Payment[] payments = {new Payment("А.А.Иванов", 1,2, 2000, 10000),
                new Payment("А.А.Иванов", 2,2, 2000, 10000),
                new Payment("Б.Г.Большаков", 1,2, 2001, 150000),
                new Payment("А.А.Иванов", 2,2, 2001, 150000),
        };
        FinanceReport test = new FinanceReport(payments, "Д.Г.Данилов", new Date(1));
        FinanceReport test1 = new FinanceReport(test);

        test.editPayment(1,"А.А.Иванов", 2,2, 2000, 15000);

        assertEquals("Плательщик: А.А.Иванов D/M/Y:2/2/2000 Сумма: 100 руб. 0 коп.",
                test1.getPayment(1).toString());

    }
}