public class PieceProduct extends Product implements MyWeight{

     // Класс «Штучный товар». Штучный товар хранит название, описание и вес одной штуки товара (в кг)
     // Конструктор по всем трем полям
    private double weight;
    public PieceProduct(String name, String description, double weightOfProduct) throws ProductException{
        super(name, description);
        if(weightOfProduct <= 0){
            throw new ProductException("Вес отрицательный", weightOfProduct);
        }
        weight = weightOfProduct;
    }

    public double getWeight(){
        return weight;
    }

    public void setWeight(double weight){
        this.weight = weight;
    }
}