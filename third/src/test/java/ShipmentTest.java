import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShipmentTest {
    @Test
    void getWeight() throws ProductException, PackingException {
        MyPackingWeightProduct[] arr = new  MyPackingWeightProduct[4];
        Packing pack = new Packing("Упаковка 1", 0.001);
        Packing pack1 = new Packing("Упаковка 2", 0.002);
        Product goods = new Product("Товар", "Описание");
        PieceProduct pieceGoods = new PieceProduct("Штучный товар", "и его описание", 0.2);
        arr[0] = new PackingPieceProduct(pack,3, pieceGoods);
        arr[1] = new PackingWeightProduct(goods, 0.5, pack1);
        arr[2] = new PackingPieceProduct(pack,5, pieceGoods);
        arr[3] = new PackingPieceProduct(pack1, 6, pieceGoods);
        Shipment shipment = new Shipment("Сундук", arr);
        assertEquals(3.306, shipment.getWeight());

    }

}