import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PackingPieceProductTest {

    @Test
    void testGetCountOfGoods() throws ProductException, PackingException {
        PackingPieceProduct test = new PackingPieceProduct(
                new Packing("Название упаковки", 0.001), 7,
                new PieceProduct("Мыло", "Банное", 0.05));
        assertEquals(7, test.getCountOfPieceProduct());

    }

    @Test
    void testGetWeightOfAllProduct() throws PackingException, ProductException {
        PackingPieceProduct test = new PackingPieceProduct(
                new Packing("Название упаковки", 0.001), 5,
                new PieceProduct("Мыло", "Банное", 0.05));
        assertEquals(0.25, test.getWeightWithoutPacking());
    }

    @Test
    void TestGetWeightOfAllProductWithPacking() throws ProductException, PackingException {
        PackingPieceProduct test = new PackingPieceProduct(new Packing("Название упаковки", 0.001),
                6, new PieceProduct("Мыло", "Банное", 0.05));
        assertTrue(Math.abs(0.301- test.getWeight()) < 10e-5);
    }

}