import java.util.Arrays;
import java.util.ArrayList;

public class FinanceReportProcessor {
    private static final String[] MONTHS = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август",
            "Сентябрь","Октябрь", "Ноябрь", "Декабрь"};

    //Получение платежей всех людей, чья фамилия начинается на указанный символ

    public static FinanceReport SelectedChar(FinanceReport financeReport, char fioBegin)
            throws IllegalArgumentException{
        ArrayList<Payment> arr = new ArrayList<>();
        int counter = 0;

        for(int i = 0; i < financeReport.getArr().length; i++){
            char checkedChar = financeReport.getArr()[i].getFullName().charAt(0);
            if(fioBegin == checkedChar){
                arr.add(financeReport.getPayment(i));
                counter++;
            }
        }
        if(counter > 0) {
            return new FinanceReport(arr.toArray(Payment[]::new), financeReport.getOrgFio(), financeReport.getDate());
        }
        else{
            throw new IllegalArgumentException("Массив не содержит таких платежей");
        }
    }
    //вывод всех платежей, размер которых меньше заданной величины
    public static FinanceReport getPayments(FinanceReport financeReport, int amount)
            throws IllegalArgumentException{
        ArrayList<Payment> arr = new ArrayList<>();
        int counter = 0;
        int checkedAmount;
        for(int i = 0; i < financeReport.getArr().length; i++){
            checkedAmount = financeReport.getArr()[i].getSum();
            if(checkedAmount < amount){
                arr.add(financeReport.getPayment(i));
                counter++;
            }
        }
        if(counter > 0) {
            return new FinanceReport(arr.toArray(Payment[]::new), financeReport.getOrgFio(), financeReport.getDate());
        }
        else{
            throw new IllegalArgumentException("Массив не содержит таких платежей");
        }
    }
    //список названий месяцев, в которых не было ни одного платежа в течение этого года
    public static String[] getMonths(FinanceReport fin, int year){
        ArrayList<String> months = new ArrayList<>(Arrays.asList(MONTHS));
        int checkYear, checkMonth;
        for(int i = 0; i < fin.getArr().length; i++){
            checkYear = fin.getArr()[i].getYear();
            if(checkYear == year){
                checkMonth = fin.getArr()[i].getMonth();
                months.remove(MONTHS[checkMonth-1]);
            }
        }
        return months.toArray(String[]::new);
    }
}
