import java.util.Scanner;

public class lab11 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size;
        int count = 0;
        double a, b;
        System.out.println("Введите количество элементов массива");
        size = in.nextInt();
        double array[] = new double[size];
        System.out.println("Введите элементы массива");
        for (int i = 0; i < size; i++) {
            array[i] = in.nextDouble();
        }
        System.out.println("Введите отрезок [a, b] ");
        a = in.nextDouble();
        b = in.nextDouble();
        for (double i = a; i <= b; i++) {
            count++;
        }
        System.out.println(" Количество чисел принадлежащих отрезку [a, b] равно " + count + " ");
    }
}