public class Shipment{
    private String description;
    private MyPackingWeightProduct[] shipment;


     // Конструктор по произвольному набору упаковок товаров

    public Shipment(String description,MyPackingWeightProduct ... arr) throws ProductException {
        if(description.isEmpty()){
            throw new ProductException("Нет названия товара");
        }
        shipment = arr;
        this.description = description;
    }
    // получить массу всей партии

    public double getWeight() {
        double result = 0;
        for(MyPackingWeightProduct temp: shipment){
            result += temp.getWeight();
        }
        return result;
    }

    public MyPackingWeightProduct[] getShipment(){
        return shipment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

