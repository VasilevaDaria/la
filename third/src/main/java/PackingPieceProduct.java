public class PackingPieceProduct implements MyPackingWeightProduct{
    // класс упакованный штучный товар
    private Packing packing;
    private int countOfPieceProduct;
    private PieceProduct pieceProduct;

    public PackingPieceProduct(Packing packing, int countOfPieceProduct, PieceProduct pieceProduct) throws ProductException {
        if(countOfPieceProduct <= 0){
            throw new ProductException("Кол-во товаров отрицательно!", countOfPieceProduct);
        }
        this.packing = packing;
        this.countOfPieceProduct = countOfPieceProduct;
        this.pieceProduct = pieceProduct;
    }

    int getCountOfPieceProduct(){
        return countOfPieceProduct;
    }

    // получить массу брутто (упаковки и всех единиц товара вместе)

    @Override
    public double getWeight() {
        return getWeightWithoutPacking() + packing.getWeight();
    }

   // получить массу нетто(суммарный вес всех единиц товара)

    public double getWeightWithoutPacking(){
        return countOfPieceProduct*pieceProduct.getWeight();
    }

    @Override
    public Product getProduct() {
        return pieceProduct;
    }


}
