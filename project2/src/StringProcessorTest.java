
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
class StringProcessorTest {
    @Test
    void testСopyWithCorrectParameter() {
        String test = "Предложение для проверки";

        assertEquals("Предложение для проверкиПредложение для проверкиПредложение для проверки",
                StringProcessor.Сopy(test, 3));
        System.out.println( StringProcessor.Сopy(test, 3));
    }
    @Test
    void testСopyWithWrongParameter() {
        String test = "Предложение для проверки";

        assertThrows(IllegalArgumentException.class,()-> StringProcessor.Сopy(test, -1));
    }
    @Test
    void testnumberWithCorrectSubstring() {
        String test = "Предложение для для проверки";
        assertEquals(2,
                StringProcessor.number(test, "для"));
        System.out.println( StringProcessor.number(test, "для"));
    }
    @Test
    void testnumberWithVeryLongSubstring() {
        String test = "Предложение для для проверки";

        assertThrows(IllegalArgumentException.class,
                ()-> StringProcessor.number(test,"Предложение для для проверки абвгд"));

    }
    @Test
    void testreplace() {
        String test = " 1 + 2 = 3";
        assertEquals(" один + два = три",
                StringProcessor.replace(test));
        System.out.println( StringProcessor.replace(test));
    }
    @Test
    void testdeleteSymbol() {
        StringBuilder test = new StringBuilder("Предложение");
        assertEquals("рдоеи", StringProcessor.deleteSymbol(test).toString());

    }
}