import org.junit.jupiter.api.Test;
import static java.lang.Math.random;
import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.*;

class Vector3DArrayTest {

    @Test
    void testGetLength() {
        Vector3DArray arr = new Vector3DArray(5);

        assertEquals(5, arr.getLength());
        System.out.println(arr.getLength());
    }

    @Test
    void testFindMax() {
        Vector3DArray arr = new Vector3DArray(6);
        for(int i = 0; i < 6; i++) {
            arr.editElem(i, new Vector3D(random() % 10, random() % 10, random() % 10));
        }
        arr.editElem(4, new Vector3D(10, 10, 10));

        assertEquals(sqrt(300), arr.findMax());
        System.out.println(arr.findMax());
    }

    @Test
    void testFindEqualsIndex() {
        Vector3DArray arr = new Vector3DArray(6);
        for(int i = 0; i < 6; i++) {
            arr.editElem(i, new Vector3D(random() % 10, random() % 10, random() % 10));
        }

        arr.editElem(2, new Vector3D(10, 10, 10));

        assertEquals(2, arr.findEqualsIndex(new Vector3D(10, 10, 10)));
        System.out.println(arr.findEqualsIndex(new Vector3D(10, 10, 10)));
    }

    @Test
    void testSumAllVector() {
        Vector3DArray arr = new Vector3DArray(6);

        for(int i = 0; i < 6; i++) {
            arr.editElem(i, new Vector3D(0, 6, 8));
        }

        assertEquals(60, arr.sumAllVector());
        System.out.println(arr.sumAllVector());
    }

    @Test
    void testCombVector() {
        Vector3DArray arr = new Vector3DArray(5);
        arr.editElem(0, new Vector3D(1, 1, 1));
        arr.editElem(1, new Vector3D(1, 2, 2));
        arr.editElem(2, new Vector3D(0, 1, 0));
        Double[] coeffs = {1., 2., 3., 0., -1.};
        assertTrue(Vector3D.equals(new Vector3D(3, 8, 5), arr.combVector(coeffs)));
        System.out.println(arr.combVector(coeffs));
    }

    @Test
    void testShiftVector() {
        Vector3DArray arr = new Vector3DArray(3);
        arr.editElem(0, new Vector3D(1, 2, 3));
        arr.editElem(1, new Vector3D(-1, 3, -4));
        arr.editElem(2, new Vector3D(11, -2, 7));

        Point3D a = new Point3D(1,1,1);
        Point3D[] coeffs = {new Point3D(2,3,4), new Point3D(0,4,-3),new Point3D(12,-1,8)};
        Point3D[] testingArr = arr.shiftVector(a);
        assertArrayEquals(coeffs, testingArr);
        System.out.println(arr.shiftVector(a));
    }
}