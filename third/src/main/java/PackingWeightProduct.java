public class PackingWeightProduct implements MyPackingWeightProduct{
    // Класс упакованный весовой товар
    private Packing packing;
    private WeightProduct weightProduct;


     // Конструктор по товару, массе товара и упаковке

    public PackingWeightProduct(Product product, double weightOfProduct, Packing packing) throws ProductException {
        this.packing = packing;
        this.weightProduct = new WeightProduct(product, weightOfProduct);
    }

  // получить массу брутто (упаковки и товара вместе)

    @Override
    public double getWeight() {
        return packing.getWeight() + weightProduct.getWeight();
    }

    // получить массу нетто (только товара)

    public double getWeightWithoutPacking(){
        return weightProduct.getWeight();
    }

    @Override
    public Product getProduct() {
        return weightProduct;
    }

}
