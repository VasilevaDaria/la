import java.util.Scanner;

public class lab5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float a, b, h, i;
        System.out.println("Введите начальный предел табулирования a");
        a = in.nextFloat();
        System.out.println("Введите конечный предел табулирования b");
        b = in.nextFloat();
        System.out.println("Введите шаг h");
        h = in.nextFloat();
        for (i = a; i <= b; i += h) {
            double func = Math.sin(i);

            System.out.println("====================================================================");
            System.out.println("|" + i + "                      | " + func + "              ");


        }
        System.out.println("====================================================================");
        System.out.println("промежуток [" + a + ", " + b + "] шаг h=" + h + "");
    }
}
