import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PackingWeightProductTest {
    @Test
    void testGetWeightOfProduct() throws ProductException, PackingException {
        PackingWeightProduct test = new PackingWeightProduct(
                new WeightProduct("Сахар", "Тросниковый сахар", 0.5), 0.5,
                new Packing("Название упаковки", 0.001));
        assertEquals(0.5, test.getWeightWithoutPacking());
    }

    @Test
    void testGetWeightWithPacking() throws ProductException, PackingException {
        PackingWeightProduct test = new PackingWeightProduct(
                new WeightProduct("Сахар", "Тросниковый сахар", 0.5), 0.5,
                new Packing("Название упаковки", 0.003));
        assertEquals(0.503, test.getWeight());
    }

}