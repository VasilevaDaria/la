public class WeightProduct extends Product implements MyWeight{
    //Класс «Весовой товар». Весовой товар хранит название и описание
    private double weightOfProduct;
    public WeightProduct(String name, String description, double weightOfGoods) throws ProductException{
        super(name, description);
        if(weightOfGoods <= 0){
            throw new ProductException("Вес товара отрицателен", weightOfGoods);
        }
        this.weightOfProduct = weightOfGoods;
    }

    public WeightProduct(Product product, double weightOfProduct) throws ProductException {
        super(product.getName(), product.getDescription());
        if(weightOfProduct <= 0){
            throw new ProductException("Вес товара отрицателен", weightOfProduct);
        }
        this.weightOfProduct = weightOfProduct;
    }

    @Override
    public double getWeight() {
        return weightOfProduct;
    }
}

