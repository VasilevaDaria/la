import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector3DProcessorTest {

    @Test
    void testSumVector() {
        Vector3D a = new Vector3D(1, 2, 3);
        Vector3D b = new Vector3D(3, 5, 6);

        assertTrue(Vector3D.equals(new Vector3D(4, 7, 9), Vector3DProcessor.sumVector(a, b)));
    }


    @Test
    void testSubVector() {
        Vector3D a = new Vector3D(2, 3, 5);
        Vector3D b = new Vector3D(1, 2, 3);
        Vector3D c = new Vector3D(-1, -7, 0);

        assertTrue(Vector3D.equals(new Vector3D(1, 1, 2), Vector3DProcessor.subVector(a, b)));
        assertTrue(Vector3D.equals(new Vector3D(2, 9, 3), Vector3DProcessor.subVector(b, c)));
    }

    @Test
    void testScalarVector() {
        Vector3D a = new Vector3D(1, 3, 5);
        Vector3D b = new Vector3D(4, -3, 2);

        assertEquals(5, Vector3DProcessor.scalarVector(a, b));
    }

    @Test
    void testVectorProduct() {
        Vector3D a = new Vector3D(1, 2, 3);
        Vector3D b = new Vector3D(3, 1, 7);
        Vector3D c = new Vector3D(-1, -7, 0);

        assertTrue(Vector3D.equals(new Vector3D(11, 2, -5), Vector3DProcessor.vectorProduct(a, b)));
        assertTrue(Vector3D.equals(new Vector3D(49, -7, -20), Vector3DProcessor.vectorProduct(b, c)));
    }

    @Test
    void testCollVector() {
        Vector3D a = new Vector3D(1, 2, 3);
        Vector3D b = new Vector3D(2, 4, 6);
        Vector3D c = new Vector3D(3, 5, 1);

        assertTrue(Vector3DProcessor.collVector(a, b));
        assertFalse(Vector3DProcessor.collVector(b, c));
    }
}