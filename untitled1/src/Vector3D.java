import java.util.Objects;

public class Vector3D {
    private double x, y, z;


    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public Vector3D() {
        this(0, 0, 0);
    }


    public Vector3D(Point3D point1, Point3D point2) {
        this(point2.getX() - point1.getX(), point2.getY() - point1.getY(), point2.getZ() - point1.getZ());
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }


    public double getLen() {
        return Math.sqrt(x * x + y * y + z * z);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Vector3D vector3D = (Vector3D) obj;
        return Double.compare(vector3D.x, x) == 0 && Double.compare(vector3D.y, y) == 0 && Double.compare(vector3D.z, z) == 0;
    }


    public static boolean equals(Object obj, Object obj1) {
        if (obj == obj1) {
            return true;
        }
        if (obj == null || obj1 == null) {
            return false;
        }
        Vector3D vector3D1 = (Vector3D) obj;
        Vector3D vector3D2 = (Vector3D) obj1;
        return Double.compare(vector3D1.x, vector3D2.x) == 0 && Double.compare(vector3D1.y, vector3D2.y) == 0
                && Double.compare(vector3D1.z, vector3D2.z) == 0;

    }

}