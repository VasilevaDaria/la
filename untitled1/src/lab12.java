import java.util.Scanner;

public class lab12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size;
        int i;
        int count = 0;
        System.out.println("Введите количество элементов массива");
        size = in.nextInt();
        double array[] = new double[size];
        System.out.println("Введите элементы массива");
        for (i = 0; i < size; i++) {
            array[i] = in.nextDouble();
        }

        for (i = 0; i < size; i++) {
            if (array[i] % 2 == 0) {
                count++;
            }
        }
        System.out.println(" Количество четных элементов массива равно  " + count + "");
    }
}