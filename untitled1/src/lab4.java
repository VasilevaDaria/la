import java.util.Scanner;

public class lab4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a, b, c;
        System.out.println("Программа решает квадратное уравнение вида ax^2 + bx +c=0");
        System.out.println("Введите число a");
        a = in.nextDouble();
        while (a == 0) {
            System.out.println("Старший коэффициент не должен ровнятся нулю. Введите число a");
            a = in.nextDouble();
        }

        System.out.println("Введите число b");
        b = in.nextDouble();
        System.out.println("Введите число c");
        c = in.nextDouble();
        double D;
        D = b * b - 4 * a * c;
        if (D > 0) {
            double x1, x2;
            x1 = (-b + Math.sqrt(D)) / (2 * a);
            x2 = (-b - Math.sqrt(D)) / (2 * a);
            System.out.println("2 корня квадратного уровнения: х1 =" + x1 + " x2=" + x2 + "");
        } else if (D == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println("Корень квадратного уравнения x = " + x + "");
        } else if (D < 0) System.out.println("Уравнение не имет корней");
    }
}
