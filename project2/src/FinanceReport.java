import java.util.Date;

//FinanceReport, содержащий массив платежей, ФИО составителя отчета, дату создания отчета.

public class FinanceReport{

    private Payment[] arr;
    private String orgFio;
    private Date date;

    FinanceReport(Payment[] arr, String orgFio, Date date) throws IllegalArgumentException {
        if(arr == null || orgFio.isBlank() || date == null){
            throw new IllegalArgumentException("Введенные данные некоректны");
        }
        this.arr = arr;
        this.orgFio = orgFio;
        this.date = date;
    }

    //Конструктор копирования по объекту класса(после создания копии массива при изменении
    // данных в объектах исходного массива копия изменяться не должна)

    FinanceReport(FinanceReport obj){
        Payment[] payments = new Payment[obj.getArr().length];
        for(int i = 0; i < obj.getArr().length; i++){
            payments[i] = new Payment(obj.getArr()[i].getFullName(),obj.getArr()[i].getMonth(),obj.getArr()[i].getDay(),
                    obj.getArr()[i].getYear(),obj.getArr()[i].getSum());
        }
        this.arr = payments;
        this.orgFio = obj.getOrgFio();
        this.date = obj.getDate();
    }

    Payment[] getArr() {
        return arr;
    }

    String getOrgFio() {
        return orgFio;
    }

    Date getDate() {
        return date;
    }

    //Метод. получения кол-ва платежей


    public int getPaymentCount(){
        return arr.length;
    }

    //Метод изменения i-го платежа c помощью передачи параметров

    public void editPayment(int index, String fio,int day, int month, int year, int amount)throws IllegalArgumentException{
        if(index > arr.length-1){
            throw new IllegalArgumentException("Такого индекса нет в массиве");
        }
        arr[index].setDay(day);
        arr[index].setMonth(month);
        arr[index].setYear(year);
        arr[index].setSum(amount);
        arr[index].setFullName(fio);
    }

    //Метод изменения i-го платежа с помощью изменения на данные передаваймого платежа

    public void editPayment(int index, Payment payment)throws IllegalArgumentException{
        if(index > arr.length-1){
            throw new IllegalArgumentException("Такого индекса нет в массиве");
        }
        arr[index].setDay(payment.getDay());
        arr[index].setMonth(payment.getMonth());
        arr[index].setYear(payment.getYear());
        arr[index].setSum(payment.getSum());
        arr[index].setFullName(payment.getFullName());
    }

    //  Метод получения i-го платежа

    public Payment getPayment(int index){
        if(index > arr.length-1){
            throw new IllegalArgumentException("Такого индекса нет в массиве");
        }
        return arr[index];
    }

    //Метод который преобразует отчет в набор строк

    @Override
    public String toString(){
        StringBuilder s1 = new StringBuilder();
        for (Payment payment : arr) {
            s1.append(payment.toString()).append("\n");
        }
        return String.format("[Автор:%s, Дата:%s, Платежи:[\n%s]]",orgFio,date.toString(), s1.toString());
    }

}
