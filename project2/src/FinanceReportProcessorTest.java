import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Date;

class FinanceReportProcessorTest {
    Payment[] payments = {
            new Payment("Иванов И.В",7 ,12, 2019, 15230),
            new Payment("Ишаков Ю.М", 10,22, 2019, 8720),
            new Payment("Соколов Ю.Ф", 8,9, 2019, 632500),
            new Payment("Новиков Н.А", 1,31, 2019, 400),
            new Payment("Морозов В.П", 4,14, 2019, 7301),
            new Payment("Трешков А.С", 8,1, 2019, 386400),
            new Payment("Грач Н.В", 8,1, 2019, 42100),
            new Payment("Каракут Р.З", 8,1, 2019, 1475),
            new Payment("Кубрина К.Г", 11,19, 2019, 96333),
            new Payment("Сафронов Т.Н", 6,2, 2019, 7200),
    };
    FinanceReport test = new FinanceReport(payments, "Смирнов И.А", new Date(1));

    @Test
    void testSelectedChar() {
        assertEquals("[Автор:Смирнов И.А, Дата:Thu Jan 01 06:00:00 OMST 1970, Платежи:[\n" +
                "Плательщик: Иванов И.В D/M/Y:12/7/2019 Сумма: 152 руб. 30 коп.\n" +
                "Плательщик: Ишаков Ю.М D/M/Y:22/10/2019 Сумма: 87 руб. 20 коп.\n" +
                "]]", FinanceReportProcessor.SelectedChar(test, 'И').toString());
    }

    @Test
    void testGetPayments() {
        assertEquals("[Автор:Смирнов И.А, Дата:Thu Jan 01 06:00:00 OMST 1970, Платежи:[\n" +
                "Плательщик: Иванов И.В D/M/Y:12/7/2019 Сумма: 152 руб. 30 коп.\n" +
                "Плательщик: Ишаков Ю.М D/M/Y:22/10/2019 Сумма: 87 руб. 20 коп.\n" +
                "Плательщик: Новиков Н.А D/M/Y:31/1/2019 Сумма: 4 руб. 0 коп.\n" +
                "Плательщик: Морозов В.П D/M/Y:14/4/2019 Сумма: 73 руб. 1 коп.\n" +
                "Плательщик: Каракут Р.З D/M/Y:1/8/2019 Сумма: 14 руб. 75 коп.\n" +
                "Плательщик: Сафронов Т.Н D/M/Y:2/6/2019 Сумма: 72 руб. 0 коп.\n" +
                "]]", FinanceReportProcessor.getPayments(test, 16000).toString());
    }
    @Test
    void testGetMonths() {
        String[] result = FinanceReportProcessor.getMonths(test, 2019);
        String[] expectedResult = new String[]{"Февраль", "Март", "Май", "Сентябрь", "Декабрь"};
        assertArrayEquals(expectedResult, result);
    }
}