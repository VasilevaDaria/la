import java.util.Scanner;

public class lab8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size;
        System.out.println("Введите количество элементов массива");
        size = in.nextInt();
        double array[] = new double[size];
        System.out.println("Введите элементы массива");
        for (int i = 0; i < size; i++) {
            array[i] = in.nextDouble();
        }
        System.out.println("Вывод массива");
        for (int i = 0; i < size; i++) {
            System.out.print(" " + array[i]);
        }
    }
}



